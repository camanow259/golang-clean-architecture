package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	todoapp "gitlab.com/camanow259/todo-app"
	"gitlab.com/camanow259/todo-app/pkg/handler"
	"gitlab.com/camanow259/todo-app/pkg/repository"
	"gitlab.com/camanow259/todo-app/pkg/service"
)

//	@title			Todo App API
//	@version		0.1
//	@description	API for todo app

//	@host		localhost:8888
//	@BasePath	/

//	@securityDefinition.apikey	ApiKeyAuth
//	@in							header
//	@name						Authorization

func main() {
	// logrus.SetFormatter(new(logrus.JSONFormatter))

	if err := InitConfig(); err != nil {
		logrus.Fatalf("Error initializing configs: %s", err.Error())
	}

	if err := godotenv.Load(); err != nil {
		logrus.Fatalf("error loading env variables: %s", err.Error())
	}

	db, err := repository.NewPostgresDB(repository.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
		Password: os.Getenv("DB_PASSWORD"),
	})

	if err != nil {
		logrus.Fatalf("failed to initialize db: %s", err.Error())
	}

	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	srv := new(todoapp.Server)
	go func() {
		if err := srv.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil {
			logrus.Fatalf("error occured while running http server - %s", err.Error())
		}
	}()

  logrus.Print("TodoApp started")
  
  quit := make(chan os.Signal, 1)
  signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
  <- quit
  
  logrus.Print("TodoApp Shutting Down")

  // if err := srv.Shutdown(context.Background()); err != nil {
  //   logrus.Errorf("error occured on server Shutting down: %s", err.Error())
  // }
  if err := db.Close(); err != nil {
    logrus.Errorf("error occured on db connection close: %s", err.Error())
  }
}

func InitConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	return viper.ReadInConfig()
}
