package todoapp

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Server struct {
	httpServer *http.Server
}

func (s *Server) Run(port string, handler http.Handler) error {
	s.httpServer = &http.Server{
		Addr:           ":" + port,
		Handler:        handler,
		MaxHeaderBytes: 1 << 20, // 1Mb
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
	}

	return s.httpServer.ListenAndServe()
}

func (s *Server) Shutdown(ctx *gin.Context) error {
	return s.httpServer.Shutdown(ctx)
}

/*
run docker on 5431 port:
  docker run --name=todo-db -e POSTGRES_PASSWORD='qwerty' -p 5431:5432 -d --rm postgres

then run this to migrate:
  migrate -path schema/ -database 'postgres://postgres:qwerty@localhost:5431/postgres?sslmode=disable' up
*/
