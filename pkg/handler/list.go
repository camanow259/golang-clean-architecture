package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/camanow259/todo-app/internal/models"
)

//	@Summary		Create todo list
//	@Security		ApiKeyAuth
//	@Tags			lists
//	@Description	create a new list 
//	@ID				create-list
//	@Accept			json
//	@Produce		json
//	@Param			input	body		models.TodoList	true	"list info"
//	@Success		200		{integer}	integer			1
//	@Failure		400,404	{object}	errorResponse
//	@Failure		500		{object}	errorResponse
//	@Failure		default	{object}	errorResponse
//	@Router			/auth/lists [post]

func (h *Handler) createList(ctx *gin.Context)  {
  userId, err := getUserId(ctx) 

  if err != nil {
    newErrorResponse(ctx, http.StatusInternalServerError, "user id not found")
    return 
  }

  var input models.TodoList
  if err := ctx.BindJSON(&input); err != nil {
    newErrorResponse(ctx, http.StatusBadRequest, err.Error())
    return
  }

  // call service method
  id, err := h.services.TodoList.Create(userId, input)
  if err != nil {
    newErrorResponse(ctx, http.StatusInternalServerError, err.Error())
    return 
  }

  ctx.JSON(http.StatusOK, map[string]interface{}{
    "id": id,
  })
}

type getAllListsResponse struct {
  Data []models.TodoList `json:"data"`
}

func (h *Handler) getAllLists(ctx *gin.Context)  {
  userId, err := getUserId(ctx) 

  if err != nil {
    newErrorResponse(ctx, http.StatusInternalServerError, "user id not found")
    return 
  }
  
  // call service method
  lists, err := h.services.TodoList.GetAll(userId)
  if err != nil {
    newErrorResponse(ctx, http.StatusInternalServerError, err.Error())
    return 
  }

  ctx.JSON(http.StatusOK, getAllListsResponse{
    Data: lists,
  })

}

func (h *Handler) getListById(ctx *gin.Context)  {
  userId, err := getUserId(ctx)  

  listId, err := strconv.Atoi(ctx.Param("id"))
  if err != nil {
    newErrorResponse(ctx, http.StatusBadRequest, "param id is invalid")
    return 
  }

  if err != nil {
    newErrorResponse(ctx, http.StatusInternalServerError, "user id not found")
    return 
  }

  // call service method
  list, err := h.services.TodoList.GetById(userId, listId)    

  ctx.JSON(http.StatusOK, map[string]interface{}{
    "data": list,
  })
}

func (h *Handler) updateList(ctx *gin.Context)  {
  userId, err := getUserId(ctx)  

  listId, err := strconv.Atoi(ctx.Param("id"))

  if err != nil {
    newErrorResponse(ctx, http.StatusBadRequest, "param id is invalid")
    return 
  }

  var input models.UpdateListInput
  if err := ctx.BindJSON(&input); err != nil {
    newErrorResponse(ctx, http.StatusBadRequest, err.Error())
    return 
  }

  if err := h.services.TodoList.Update(userId, listId, input); err != nil {
    newErrorResponse(ctx, http.StatusInternalServerError, err.Error())
    return 
  }

  ctx.JSON(http.StatusOK, statusResponse{
    Status: "ok",
  })
  
}

func (h *Handler) deleteList(ctx *gin.Context)  {
  userId, err := getUserId(ctx)  

  listId, err := strconv.Atoi(ctx.Param("id"))
  if err != nil {
    newErrorResponse(ctx, http.StatusBadRequest, "param id is invalid")
    return 
  }

  // call service method
  err = h.services.TodoList.Delete(userId, listId) 
  if err != nil {
    newErrorResponse(ctx, http.StatusInternalServerError, err.Error())
    return 
  }

  ctx.JSON(http.StatusOK, statusResponse{
    Status: "ok",
  })}
