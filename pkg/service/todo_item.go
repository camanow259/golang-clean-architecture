package service

import (
	"gitlab.com/camanow259/todo-app/internal/models"
	"gitlab.com/camanow259/todo-app/pkg/repository"
)

type TodoItemService struct {
  repo repository.TodoItem 
  listRepo repository.TodoList
}

func NewTodoItemService(repo repository.TodoItem, listRepo repository.TodoList) *TodoItemService {
  return &TodoItemService{
    repo: repo,
    listRepo: listRepo,
  }
}

func (s *TodoItemService) Create(userId, listId int, input models.TodoItem) (int, error) {
  _, err := s.listRepo.GetById(userId, listId)
  if err != nil {
    // list does not exist or does not belong to user
    return 0, err
  }
   
  return s.repo.Create(listId, input)
}

func (s *TodoItemService) GetAll(userId, listId int) ([]models.TodoItem, error) {
  return s.repo.GetAll(userId, listId)
}

func (s *TodoItemService) GetById(userId,  itemId int) (models.TodoItem, error) {
  return s.repo.GetById(userId, itemId)
}

func (s *TodoItemService) Delete(userId,  itemId int) (error) {
  return s.repo.Delete(userId, itemId)
}

func (s *TodoItemService) Update(userId, listId int, input models.UpdateItemInput) error{
  if err := input.Validate(); err != nil {
    return err
  }
  return s.repo.Update(userId, listId, input)
}
